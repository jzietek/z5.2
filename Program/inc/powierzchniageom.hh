#ifndef POWIERZCHNIAGEOM_HH
#define POWIERZCHNIAGEOM_HH

#include "MacierzRot3D.hh"
#include "Wektor3D.hh"

#include <vector>
#include <iostream>
#include <fstream>

/**
 * @brief Klasa modelująca powierzchnie geometryczną.
 * 
 */
class powierzchniageom {
    public:
    /**
     * @brief Ciało klasy.
     * 
     */
    std::vector<Wektor3D> PunktyGlob;
    /**
     * @brief Construct a new powierzchniageom object
     * 
     */
    powierzchniageom(){};

    /**
     * @brief Construct a new powierzchniageom object
     * 
     * @param NazwaPliku 
     */
    powierzchniageom(const std::string & NazwaPliku){
        std::ifstream Strmwej(NazwaPliku);
        
        double x, y, z;
        while (!Strmwej.eof()){
            Strmwej>>x;
            Strmwej>>y;
            Strmwej>>z;
            Wektor3D wek(x,y,z);
            PunktyGlob.push_back(wek);
        }}

    /**
     * @brief 
     * 
     * @return const char* 
     */
    virtual const char* WezNazweTypuObiektu(){return "PowierzchniaGeom";}

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
    virtual double Xmin ();

    /**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
    virtual double Xmax ();

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy y.
     * 
     * @return double 
     */
    virtual double Ymin ();

    /**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
    virtual double Ymax ();

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
    virtual double Zmin ();

    /**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
    virtual double Zmax ();
};


#endif