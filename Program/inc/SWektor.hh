#ifndef SWEKTOR_HH
#define SWEKTOR_HH
#include <iostream>


/**
 * @brief Szablon wektora.
 * 
 * @tparam STyp 
 * @tparam SWymiar 
 */
template <typename STyp, int SWymiar>
class SWektor {
    /**
     * @brief Pole przechowujące informacje o aktualnej ilosci wektorów.
     * 
     */
    static int _IloscObiektow_Aktualna;

    /**
     * @brief Pole przechowujące informacje o łącznej ilosci wektorów.
     * 
     */
    static int _IloscObiektow_Laczna;

    /**
     * @brief Ciało wektora.
     * 
     */
    STyp  _Wsp[SWymiar];
    
  public:

  /**
   * @brief Construct a new SWektor object.
   * 
   */
    SWektor()
     {
       ++_IloscObiektow_Aktualna; ++_IloscObiektow_Laczna;
       for (STyp &Wsp: _Wsp) Wsp = 0;
     }

     /**
      * @brief Construct a new SWektor object.
      * 
      * @param W 
      */
    SWektor(const SWektor<STyp,SWymiar> &W)
     {
       ++_IloscObiektow_Aktualna; ++_IloscObiektow_Laczna;
       *this = W;
     }

     /**
      * @brief Destroy the SWektor object.
      * 
      */
    ~SWektor() { --_IloscObiektow_Aktualna; }
  
    /**
     * @brief Umożliwia zwrócenie wartości z pola prywatnego.
     * 
     * @param Ind 
     * @return STyp 
     */
    STyp  operator [] (unsigned int Ind) const { return _Wsp[Ind]; }

    /**
     * @brief Umożliwia wpisanie wartości do pola prywatnego.
     * 
     * @param Ind 
     * @return STyp& 
     */
    STyp &operator [] (unsigned int Ind)       { return _Wsp[Ind]; }

    /**
     * @brief Przeciążenie operatora +=.
     * 
     * @param Skladnik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator += (const SWektor<STyp,SWymiar> &Skladnik);

    /**
     * @brief Przeciążenie operatora dodawania.
     * 
     * @param Skladnik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator + (const SWektor<STyp,SWymiar> &Skladnik) const;

    /**
     * @brief Przeciążenie operatora -=.
     * 
     * @param Odjemnik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator -= (const SWektor<STyp,SWymiar> &Odjemnik); 

    /**
     * @brief Przeciążenie operatora odejmowania.
     * 
     * @param Odjemnik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator - (const SWektor<STyp,SWymiar> &Odjemnik) const;

    /**
     * @brief Przeciążenie *=.
     * 
     * @param Mnoznik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator *= (const STyp &Mnoznik);

    /**
     * @brief Przeciążenie mnożenia.
     * 
     * @param Mnoznik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator *  (const STyp &Mnoznik) const;

    /**
     * @brief Przciążenie /=.
     * 
     * @param Dzielnik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator /= (const STyp &Dzielnik);

    /**
     * @brief Przeciążenie dzielenia.
     * 
     * @param Dzielnik 
     * @return SWektor<STyp,SWymiar> 
     */
    SWektor<STyp,SWymiar> operator /  (const STyp &Dzielnik) const;  

  /*!
    * Oblicza kwadrat normy wektora.
    *
    * Zwracana warto¶æ:
    *   kwadrat normy wektora.
    */
    double Norma2() const;

    /**
     * @brief Metoda zwracająca akttualną liczbę wektorów.
     * 
     * @return int 
     */
    static int WezIloscObiektow_Aktualna() { return _IloscObiektow_Aktualna; }

    /**
     * @brief Metoda zwracająca łączną ilość obiektów.
     * 
     * @return int 
     */
    static int WezIloscObiektow_Laczna() { return _IloscObiektow_Laczna; }  
};

template <typename STyp, int SWymiar>
int SWektor<STyp,SWymiar>::_IloscObiektow_Aktualna = 0;

template <typename STyp, int SWymiar>
int SWektor<STyp,SWymiar>::_IloscObiektow_Laczna = 0;





template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator *= (const STyp &Mnoznik)
{
  for (unsigned int Ind = 0; Ind < SWymiar; ++Ind) (*this)[Ind] *= Mnoznik;
  return *this;
}


template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator * (const STyp &Mnoznik) const
{
  SWektor<STyp,SWymiar>  Wynik = *this;
  return Wynik *= Mnoznik;
}




template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator /= (const STyp &Dzielnik)
{
  for (unsigned int Ind = 0; Ind < SWymiar; ++Ind) (*this)[Ind] /= Dzielnik;
  return *this;
}


template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator / (const STyp &Dzielnik) const
{
  SWektor<STyp,SWymiar>  Wynik = *this;
  return Wynik /= Dzielnik;
}



template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator += (const SWektor<STyp,SWymiar> &Skladnik)
{
  for (unsigned int Ind = 0; Ind < SWymiar; ++Ind) (*this)[Ind] += Skladnik[Ind];
  return *this;
}



template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator + (const SWektor<STyp,SWymiar> &Skladnik) const
{
  SWektor<STyp,SWymiar>  Wynik = *this;
  return Wynik += Skladnik;
}



template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator -= (const SWektor<STyp,SWymiar> &Odjemnik)
{
  for (unsigned int Ind = 0; Ind < SWymiar; ++Ind) (*this)[Ind] -= Odjemnik[Ind];
  return *this;
}



template <typename STyp, int SWymiar>
SWektor<STyp,SWymiar> SWektor<STyp,SWymiar>::operator - (const SWektor<STyp,SWymiar> &Odjemnik) const
{
  SWektor<STyp,SWymiar>  Wynik = *this;
  return Wynik -= Odjemnik;
}




/*!
 * Oblicza kwadrat normy wektora.
 *
 * Zwracana wartość:
 *   kwadrat normy wektora.
 */
template <typename STyp, int SWymiar>
double SWektor<STyp,SWymiar>::Norma2() const
{
  double  Wynik;
  for (unsigned int Ind = 0; Ind < SWymiar; ++Ind) Wynik += (*this)[Ind]*(*this)[Ind];
  return Wynik;
}




template <typename STyp, int SWymiar>
std::istream& operator >> (std::istream &StrmWyj, SWektor<STyp,SWymiar>& W)
{
  for (unsigned int Ind = 0; Ind < SWymiar; ++Ind) {
    StrmWyj >> W[Ind];
  }  
  return StrmWyj;
}



template <typename STyp, int SWymiar>
std::ostream& operator << (std::ostream &StrmWyj, const SWektor<STyp,SWymiar>& W)
{
  for (unsigned int Ind = 0; Ind < SWymiar; ++Ind) {
    StrmWyj << " " << W[Ind];
  }  
  return StrmWyj;
}

#endif
