#ifndef SMACIERZ_HH
#define SMACIERZ_HH

#include <iomanip>
#include <algorithm>
#include "SWektor.hh"


template <typename Typ, unsigned int Rozmiar>
class SMacierz {
   /*
    * Tablica elementów macierzy.
    */
    SWektor<Typ,Rozmiar>  _Wiersz[Rozmiar];
  public:
  /*!
   * \brief Inicjalizuje macierez jako macierz jednostkową
   */
  SMacierz() { UczynJednostkowa(); }
  /*!
   * Udostępnia do odczytu element macierzy.
   *
   * Parametry:
   *   Wiersz - indeks wiersza, z którego ma być wybrany żądany element.
   *            Indeksowanie rozpoczyna się od 0 do 2.
   *   Kolumna - indeks kolumny, w której znajduje się żądany element.
   *            Indeksowanie rozpoczyna się od 0 do 2.
   * 
   * Warunki wstępne:
   *   Wiersz, Kolumna < 3.
   *
   * Zwracana wartość:
   *   wartość elementu maicerzy o podanym numerze kolumny.
   */
  Typ operator () (unsigned int Wiersz, unsigned int Kolumna) const
                                    { return _Wiersz[Wiersz][Kolumna]; }

  /*!
   * Udostępnia odczytu i zapisu element macierzy.
   *
   * Parametry:
   *   Wiersz - indeks wiersza, z którego ma być wybrany żądany element.
   *            Indeksowanie rozpoczyna się od 0 do 2.
   *   Kolumna - indeks kolumny, w której znajduje się żądany element.
   *            Indeksowanie rozpoczyna się od 0 do 2.
   * 
   * Warunki wstępne:
   *   Wiersz, Kolumna < 3.
   *
   * Zwracana wartość:
   *   referencja do elementu macierzy o podanym numerze kolumny.
   */
  Typ& operator () (unsigned int Wiersz, unsigned int Kolumna)
                                    { return _Wiersz[Wiersz][Kolumna]; }

  /*!
   * Mnoży wektor przez macierz zgodnie z ogólnie przyjętą definicją
   * tego typu operacji.
   * 
   * Parametry:
   *   W - wektor, który ma być przemnożony przez macierz.
   *
   * Zwracana wartość:
   *   Wektor będący wynikiem przemnożenia wektora W przez macierz.
   */
   SWektor<Typ,Rozmiar> operator * (const SWektor<Typ,Rozmiar>& W) const;

   SMacierz<Typ,Rozmiar> operator * (const SMacierz<Typ,Rozmiar>& W) const;


  /*
   * Wylicza wyznacznik macierzy.
   *
   * Zwaracana wartość:
   *   Wartość wyznacznika macierzy.
   */
   Typ Wyznacznik() const;
  /*!
   *
   */
  bool SprowadzDoTrojkatnej(int &ZnakPerm);
  /*
   * Wstawia do nowe wartości dla zadanej kolumny.
   *
   * Parametry:
   *    Ind_Kol - indeks kolumny. Dopuszczalne warotści to: 0, 1, 2.
   *    W - wektor zawierający nowe wartości dla zadanej kolumy.
   *        Jego wspó³rzędne x, y, z, odnoszą się odpowiednio do
   *        wiersza o indeksach 0, 1, 2.
   *
   * Zwraca wartość:
   *    true - gdy indeks "Ind_Kol" ma jedną z dopuszczalnych wartości,
   *           co równoznaczne jest z poprawnym wykonaniem operacji.
   *    false - w przypadku przeciwnym.
   */
   bool WstawKolumne(unsigned int Ind_Kol, const SWektor<Typ,Rozmiar>& W);

  /*
   * Wstawia do nowe wartości dla zadanego wiersza.
   *
   * Parametry:
   *    Ind_Wier - indeks wiersza. Dopuszczalne warotści to: 0, 1, 2.
   *    W - wektor zawierający nowe wartości dla zadanego wiersza.
   *        Jego wspó³rzędne x, y, z, odnoszą się odpowiednio do
   *        kolumn o indeksach 0, 1, 2.
   *
   * Zwraca wartość:
   *    true - gdy indeks "Ind_Wier" ma jedną z dopuszczalnych wartości,
   *           co równoznaczne jest z poprawnym wykonaniem operacji.
   *    false - w przypadku przeciwnym.
   */
   bool WstawWiersz(unsigned int Ind_Wier, const SWektor<Typ,Rozmiar>& W);


  /*!
   * \brief Transponuje daną macierz
   */
   void Transponuj();
  /*!
   * \brief Inicjalizuje macierz jednostkowa
   */
   void UczynJednostkowa();

  //SWektor<Typ, Rozmiar> operator * (const SWektor<Typ, Rozmiar> vec)const;
};



template <typename Typ, unsigned int Rozmiar>
void SMacierz<Typ,Rozmiar>::UczynJednostkowa()
{
  for (unsigned int Kol = 0; Kol < Rozmiar; ++Kol) {
    for (unsigned int Wie = 0; Wie < Rozmiar; ++Wie) (*this)(Wie,Kol) = 0;
  }
  for (unsigned int Kol = 0; Kol < Rozmiar; ++Kol) (*this)(Kol,Kol) = 1;
}


/*template<typename Typ, unsigned int Rozmiar>
SWektor<Typ, Rozmiar> SMacierz<Typ, Rozmiar>::operator * (const SWektor<Typ, Rozmiar> vec)const{
    SWektor<Typ, Rozmiar> result;
    for (unsigned int i = 0; i < Rozmiar; i++)
    {
        result[i]=this->_Wiersz[i]&vec;
    }
    return result;
}*/



/*
 * Czyta ze strumienia wejściowego macierz. Zak³ada się, że kolejne
 * elementy macierzy podawane są w kolejności: M(0,0) M(0,1) M(0,2)
 * M(1,0), M(1,1) itd.
 *
 * Parametry:
 *   StrmWej - strumieñ wejściowy, z którego wczytywane są kolejne
 *             elementy macierzy.
 *   M - reprezentuje macierz, do której wczytywane są poszczególne
 *       pola
 *
 * Warunki wstępne:
 *   W strumieniu znajduje się kolejnych 9 liczb typu double oraz stan
 *   strumienia pozwala zrealizować operację odczytu.
 *
 * Warunki koñcowe:
 *   Ze strumienia zosta³o odczytanych 9 liczb.
 *
 * Zwracana wartość:
 *   Referencja do strumienia wejściowego, z którego dokonany zosta³
 *   odczyt.
 */
template <typename Typ, unsigned int Rozmiar>
std::istream& operator >> (std::istream &  StrmWej,  SMacierz<Typ,Rozmiar> &  M )
{
  SWektor<Typ,Rozmiar>  W;

  for (unsigned int i = 0; i < Rozmiar; ++i) {
    StrmWej >> W;
    M.WstawKolumne(i,W);
    // StrmWej >> M[i];
  }
  return StrmWej;
}



/*
 * Zapisuje do strumienia weyściowego macierz. Wartości kolejnych
 * elementów macierzy wyświetlane są w następującej kolejności:
 *   M(0,0) M(0,1) M(0,2)
 *   M(1,0) M(1,1) M(1,2)
 *   M(2,0) M(2,1) M(2,2)
 *
 * Parametry:
 *   StrmWyj - strumieñ wyjściowy, do którego zapisywane są kolejne
 *             elementy macierzy.
 *   M - reprezentuje macierz, której elementy są zapisywane do strumienia
 *       wyjściowego.
 *
 * Warunki wstępne:
 *   Stan strumienia pozwala zrealizować operację zapisu.
 *
 * Warunki koñcowe:
 *   Do strumienia zosta³o odczytanych 9 liczb po 3 w jednej linii.
 *
 * Zwracana wartość:
 *   Referencja do strumienia wyjściowego, do którego zapise zosta³y
 *   elementy macierzy.
 */
template <typename Typ, unsigned int Rozmiar>
std::ostream& operator << (std::ostream &  StrmWyj, const SMacierz<Typ,Rozmiar> &  M )
{
  for (unsigned int i = 0; i < Rozmiar; ++i) {
    for (unsigned int j=0; j < Rozmiar; ++j) StrmWyj << std::setw(7) << M(i,j);
    StrmWyj << std::endl;
  }
  return StrmWyj;
}



template <typename Typ, unsigned int Rozmiar>
bool SMacierz<Typ,Rozmiar>::WstawKolumne(unsigned int Ind_Kol, const SWektor<Typ,Rozmiar>& W)
{
  if (Ind_Kol >= Rozmiar) return false;
  for (unsigned int i = 0; i < Rozmiar; ++i) (*this)(i,Ind_Kol) = W[i];
  return true;
}


template <typename Typ, unsigned int Rozmiar>
bool SMacierz<Typ,Rozmiar>::WstawWiersz(unsigned int Ind_Wier, const SWektor<Typ,Rozmiar>& W)
{
  if (Ind_Wier >= Rozmiar) return false;
  _Wiersz[Ind_Wier] = W;
  return true;
}



template <typename Typ, unsigned int Rozmiar>
bool SMacierz<Typ,Rozmiar>::SprowadzDoTrojkatnej(int &ZnakPerm)
{
  ZnakPerm = 1;
  for (unsigned int Kol=0; Kol < Rozmiar-1; ++Kol) {
    if (_Wiersz[Kol][Kol] == 0) {
      for (unsigned int Wie=Kol+1; Wie < Rozmiar; ++Wie) {
	if (_Wiersz[Wie][Kol] == 0) continue;
	std::swap(_Wiersz[Kol],_Wiersz[Wie]);
	if (((Wie-Kol) % 2) == 1) ZnakPerm = -ZnakPerm;
	break;
      }
      if (_Wiersz[Kol][Kol] == 0) return false;
    }
    SWektor<Typ,Rozmiar> Wie_Kas = _Wiersz[Kol]/_Wiersz[Kol][Kol];
    // std::cout << "             Kol: " << Kol << std::endl;
    // std::cout << "    Wiersz przed: " << _Wiersz[Kol] << std::endl; //TMP
    // std::cout << "   Elem redukcji: " << _Wiersz[Kol][Kol] << std::endl; //TMP
    // std::cout << " Wiersz kasujący: " << Wie_Kas << std::endl; //TMP
    // std::cout << std::endl;
    for (unsigned int Wie=Kol+1; Wie < Rozmiar; ++Wie) {
      // std::cout << "          Mnoznik: " << _Wiersz[Wie][Kol] << std::endl;
      // std::cout << "  Wiersz redukcji: " << Wie_Kas*_Wiersz[Wie][Kol] << std::endl; //TMP
      _Wiersz[Wie] -= Wie_Kas*_Wiersz[Wie][Kol];
    } 
  }
  return true;
}


template <typename Typ, unsigned int Rozmiar>
Typ SMacierz<Typ,Rozmiar>::Wyznacznik() const
{
  Typ  WartWyzn(1);
  SMacierz<Typ,Rozmiar>   MacRob = *this;
  /*
  int  ZnakPerm_ij, ZnakPerm_ijk;


  for (unsigned int i=0; i < Rozmiar; ++i) {
    for (unsigned int j=0; j < Rozmiar; ++j) {
      if (i == j) continue;
      ZnakPerm_ij = (i > j) ? -1 : 1;

      for (unsigned int k = 0;  k < Rozmiar; ++k) {
	if (k == i || k == j) continue;
        ZnakPerm_ijk = ZnakPerm_ij;
        if (i > k) ZnakPerm_ijk = -ZnakPerm_ijk;
        if (j > k) ZnakPerm_ijk = -ZnakPerm_ijk;

        WartWyzn += (*this)(0,i)*(*this)(1,j)*(*this)(2,k)*ZnakPerm_ijk;
      } 
    }
  }
  */
  int  ZnakPerm;
  if (!MacRob.SprowadzDoTrojkatnej(ZnakPerm)) return Typ(0);
  //  std::cout << "  Macierz trojkatna: " << std::endl;
  //  std::cout << MacRob << std::endl
  //	    << std::endl;

  for (unsigned int Idx=0; Idx < Rozmiar; ++Idx) {
    WartWyzn *= MacRob(Idx,Idx);
  }
  return WartWyzn*ZnakPerm;
}






template <typename Typ, unsigned int Rozmiar>
SWektor<Typ,Rozmiar> SMacierz<Typ,Rozmiar>::operator * (const SWektor<Typ,Rozmiar>& W) const
{
  SWektor<Typ,Rozmiar>  W_wynik;

  for (unsigned int i = 0;  i < Rozmiar;  ++i) {
    W_wynik[i] = 0;
    for (unsigned int j = 0;  j < Rozmiar;  ++j)  W_wynik[i] += (*this)(i,j)*W[j];
  }
  return W_wynik;
}




template <typename Typ, unsigned int Rozmiar>
SMacierz<Typ,Rozmiar> SMacierz<Typ,Rozmiar>::operator * (const SMacierz<Typ,Rozmiar>& M_arg) const
{
  SMacierz<Typ,Rozmiar>  M_wynik;

  for (unsigned int Kol = 0;  Kol < Rozmiar;  ++Kol) {
    for (unsigned int Wie = 0;  Wie < Rozmiar;  ++Wie) {
      M_wynik(Kol,Wie) = 0;
      for (unsigned int Idx = 0;  Idx < Rozmiar;  ++Idx) {
        M_wynik(Kol,Wie) += (*this)(Kol,Idx)*M_arg(Idx,Wie);
      }
    }
  }
  return M_wynik;
}




template <typename Typ, unsigned int Rozmiar>
void SMacierz<Typ,Rozmiar>::Transponuj()
{
  for (unsigned int Kol = 0; Kol < Rozmiar; ++Kol) {
    for (unsigned int Wie = Kol; Wie < Rozmiar; ++Wie) {
      std::swap((*this)(Kol,Wie),(*this)(Wie,Kol));
    }
  }
}

#endif