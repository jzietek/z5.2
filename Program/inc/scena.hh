#ifndef SCENA_HH
#define SCENA_HH

#include "dron.hh"
#include "powierzchnia.hh"
#include "prostopadloscian.hh"
#include "woda.hh"
#include "pret.hh"
#include "obrys.hh"

#include <list>

/**
 * @brief Klasa modelująca scenę.
 * 
 */
class scena{
    public:

    /**
     * @brief Przechowywana powierzchnia wody.
     * 
     */
    woda Woda;

    /**
     * @brief Dron, wraz ze śrubami.
     * 
     */
    dron Dron;

    /**
     * @brief Prostopadłościan, przechowujący razem współrzędne drona i śrub.
     * 
     */
    obrys dronplussruby;

    /**
     * @brief Lista elementów sceny.
     * 
     */
    std::list<powierzchniageom*> lista_elem_sceny;

    /**
     * @brief Metoda wczytuje obiekty, które później będa wyrysowane, oprócz przeszkód.
     * 
     * @return std::fstream 
     */
    std::fstream WczytajBryly();

    /**
     * @brief Metoda wypisuje do konkretnych plków wczytane obiekty.
     * 
     * @return std::ofstream 
     */
    std::ofstream WypiszBryly();

    /**
     * @brief Metoda wypisuje menu.
     * 
     */
    void WypiszMenu();

    /**
     * @brief Metoda dodaje elementy na listę.
     * 
     */
    void WczytajListe();

    /**
     * @brief Metoda wczytuje obrys z pliku.
     * 
     */
    void WczytajObrys();

    /**
     * @brief Metoda wypisuje obrys drona do sceny.
     * 
     */
    void WypiszObrys();

    /**
     * @brief Metoda sprawdza, czy dojdzie do zderzenia z przeszkodą.
     * 
     * @return true 
     * @return false 
     */
    bool CzyZderzenie();
};




#endif