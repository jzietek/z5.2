#ifndef MACIERZROT3D_HH
#define MACIERZROT3D_HH


#include <SMacierz.hh>

#define ROZM_MAC   3

/**
 * @brief Klasa modelująca macierz rotacji.
 * 
 */
class MacierzRot3D: public SMacierz<double,ROZM_MAC> {
  public:
  /**
   * @brief Construct a new Macierz Rot 3 D object
   * 
   */
   MacierzRot3D() = default;
   
   /**
    * @brief Construct a new Macierz Rot 3 D object
    * 
    * @param Mac 
    */
   MacierzRot3D(const SMacierz<double,ROZM_MAC>& Mac):
                         SMacierz<double,ROZM_MAC>(Mac) {}
  
  /**
   * @brief Metoda ustawia macierz rotacji na macierz rotacji względem osi OX.
   * 
   * @param Ang_deg 
   */
   void UstawRotX_st(double  Ang_deg);

   /**
    * @brief Metoda ustawia macierz rotacji na macierz rotacji względem osi OY.
    * 
    * @param Ang_deg 
    */
   void UstawRotY_st(double  Ang_deg);

   /**
    * @brief Metoda ustawia macierz rotacji na macierz rotacji względem osi OZ.
    * 
    * @param Ang_deg 
    */
   void UstawRotZ_st(double  Ang_deg);
};



#endif
