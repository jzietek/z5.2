#ifndef OBRYS_HH
#define OBRYS_HH

#include "powierzchniageom.hh"

/**
 * @brief Klasa modelująca obrys drona.
 * 
 */
class obrys: public powierzchniageom{
    public:

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
    virtual double Xmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
    virtual double Xmax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy y.
     * 
     * @return double 
     */
    virtual double Ymin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
    virtual double Ymax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
    virtual double Zmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
    virtual double Zmax ()override;
};

/**
 * @brief Przeciążenie operatora wczytywania obrysu.
 * 
 * @param StrWej 
 * @param ob 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, obrys & ob);

#endif