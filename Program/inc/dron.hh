#ifndef DRON_HH
#define DRON_HH

#include "prostopadloscian.hh"
#include "SWektor.hh"
#include "MacierzRot3D.hh"
#include "sruba.hh"
#include <cmath>

/**
 * @brief Klasa modelująca drona.
 * 
 */
class dron{
    public:
    /**
     * @brief Element korpusu drona.
     * 
     */
    prostopadloscian _ElementKorpusu;

    /**
     * @brief Kąt obrócenia względem osi OZ.
     * 
     */
    double _KatOZ=0;

    /**
     * @brief Wektor, przechowujący informacje o środku drona.
     * 
     */
    SWektor<double, 3> _srodekdrona;

    /**
     * @brief Prawa śruba drona.
     * 
     */
    sruba PrawaSruba;

    /**
     * @brief Lewa śruba drona.
     * 
     */
    sruba LewaSruba;


    /**
     * @brief Metoda oblicza środek drona.
     * 
     * @return SWektor<double, 3> 
     */
    SWektor<double, 3> ObliczSrodekDrona();

    /**
     * @brief Metoda obraca drona wraz ze śrubami względem osi OZ o zadany kąt.
     * 
     * @param kat 
     */
    void ObrotOZ(const double kat);

    /**
     * @brief Metoda pozwala przesuwać się dronowi wraz ze śrubami na zadanę odległość, pod zadanym kątem, uwzględniając obrót względem osi OZ.
     * 
     * @param odleglosc 
     * @param kat 
     */
    void Przesuniecie(const double odleglosc, const double kat);

    /**
     * @brief Metoda przesuwania drona po powierzchni wody.
     * 
     * @param odleglosc 
     * @param kat 
     * @param z 
     */
    void Wyplyniecie(const double odleglosc, const double kat, const double z);

    /**
     * @brief Metoda służąca drugiemu typowi poruszania się, odpowiada za poruszanie się w dół, lub w górę.
     * 
     * @param odleglosc 
     * @param kat 
     */
    void DoGory(const double odleglosc, const double kat);

    /**
     * @brief Metoda służąca drugiemu typowi poruszania się, odpowiada za poruszanie się na boki.
     * 
     * @param odleglosc 
     * @param kat 
     */
    void NaBoki(const double odleglosc, const double kat);
};



#endif