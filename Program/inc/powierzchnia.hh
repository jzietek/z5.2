#ifndef POWIERZCHNIA_HH
#define POWIERZCHNIA_HH

#include "powierzchniageom.hh"
#include <fstream>

/**
 * @brief Klasa modelująca powierzchnię.
 * 
 */
class powierzchnia: public powierzchniageom {
    public:
    virtual const char* WezNazweTypuObiektu() override {return "Powierzchnia";}
    powierzchnia (){ }
    powierzchnia (const std::string & NazwaPliku):powierzchniageom(NazwaPliku){}

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
    virtual double Xmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
    virtual double Xmax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy y.
     * 
     * @return double 
     */
    virtual double Ymin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
    virtual double Ymax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
    virtual double Zmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
    virtual double Zmax ()override;
};


/**
 * @brief Przeciążenie wczytywania powierzchni z pliku.
 * 
 * @param StrWej 
 * @param po 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, powierzchnia & po);

#endif