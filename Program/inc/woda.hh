#ifndef WODA_HH
#define WODA_HH

#include "powierzchniageom.hh"
#include "dron.hh"
#include "Wektor3D.hh"
#include <fstream>

/**
 * @brief Klasa modelująca powierzchnię wody.
 * 
 */
class woda:public powierzchniageom {
    public:
    /**
     * @brief Metoda zwracająca nazwę klasy.
     * 
     * @return const char* 
     */
    virtual const char* WezNazweTypuObiektu()override{return "Woda";}
    
    woda (){ }
    woda (const std::string & NazwaPliku):powierzchniageom(NazwaPliku){}

    /**
     * @brief Metoda sprawdzająca, czy dron jest kierowany ponad powierzchnię wody.
     * 
     * @param Dron 
     * @return true 
     * @return false 
     */
    bool CzyWyplywa(dron & Dron);

    /**
     * @brief Metoda obliczająca średnią wysokość - z.
     * 
     * @return double 
     */
    double SrednieZ();
};

/**
 * @brief Przeciążenie wczytywania powierzchni wody.
 * 
 * @param StrWej 
 * @param wo 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, woda & wo);

#endif