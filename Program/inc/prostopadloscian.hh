#ifndef PROSTOPADLOSCIAN_HH
#define PROSTOPADLOSCIAN_HH

#include "powierzchniageom.hh"
#include "SWektor.hh"
#include <fstream>

/**
 * @brief Klasa modelująca prostopadłościan.
 * 
 */
class prostopadloscian: public powierzchniageom {
    public:

    /**
     * @brief Metoda wirtualna zwracająca nazwę klasy.
     * 
     * @return const char* 
     */
    virtual const char* WezNazweTypuObiektu() override {return "Prostopadłościan";}

    /**
     * @brief Construct a new prostopadloscian object
     * 
     */
    prostopadloscian (){ }

    /**
     * @brief Construct a new prostopadloscian object
     * 
     * @param NazwaPliku 
     */
    prostopadloscian (const std::string & NazwaPliku):powierzchniageom(NazwaPliku){}
    
    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
    virtual double Xmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
    virtual double Xmax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy y.
     * 
     * @return double 
     */
    virtual double Ymin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
    virtual double Ymax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
    virtual double Zmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
    virtual double Zmax ()override;
};

/**
 * @brief Przeciążenie operatora wczytania prostopadloscianu.
 * 
 * @param StrWej 
 * @param pr 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, prostopadloscian & pr);


/**
 * @brief Przeciążenie operatora wypisania prostopadloscianu do pliku.
 * 
 * @param StrWyj 
 * @param pr 
 * @return std::ofstream& 
 */
std::ofstream &operator <<(std::ofstream &StrWyj, const prostopadloscian & pr);

#endif