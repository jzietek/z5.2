#ifndef SRUBA_HH
#define SRUBA_HH

#include "Wektor3D.hh"
#include "MacierzRot3D.hh"
#include <fstream>
#include <vector>

/**
 * @brief Klasa modelująca śrubę.
 * 
 */
class sruba{
    public:
    /**
     * @brief Ciało śruby.
     * 
     */
    std::vector<Wektor3D> cialo;

    /**
     * @brief Środek ściany śruby.
     * 
     */
    SWektor <double, 3> srodek;
};

/**
 * @brief Przeciążenie, służące do wczytywania śrub z plików.
 * 
 * @param StrWej 
 * @param sr 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, sruba & sr);


/**
 * @brief Przeciążenie operatora, służące do wypisywania do pliku śruby.
 * 
 * @param StrWyj 
 * @param sr 
 * @return std::ofstream& 
 */
std::ofstream &operator <<(std::ofstream &StrWyj, const sruba & sr);

#endif