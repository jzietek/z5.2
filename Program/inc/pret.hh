#ifndef PRET_HH
#define PRET_HH

#include "powierzchniageom.hh"

/**
 * @brief Klasa modelująca pręta.
 * 
 */
class pret:public powierzchniageom{
    public:

    /**
     * @brief Metoda wirtualna zwracająca nazwę klasy.
     * 
     * @return const char* 
     */
    virtual const char* WezNazweTypuObiektu() override {return "Pret";}

    /**
     * @brief Construct a new pret object
     * 
     */
    pret (){ }

    /**
     * @brief Construct a new pret object
     * 
     * @param NazwaPliku 
     */
    pret (const std::string & NazwaPliku):powierzchniageom(NazwaPliku){}

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
    virtual double Xmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
    virtual double Xmax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy y.
     * 
     * @return double 
     */
    virtual double Ymin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
    virtual double Ymax ()override;

    /**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
    virtual double Zmin ()override;

    /**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
    virtual double Zmax ()override;
};


#endif