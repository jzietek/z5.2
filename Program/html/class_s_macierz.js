var class_s_macierz =
[
    [ "SMacierz", "class_s_macierz.html#a62385cf848f519a3fbca16c72c2b91df", null ],
    [ "operator()", "class_s_macierz.html#aec3b4e86829db2d0e3f4299515575baf", null ],
    [ "operator()", "class_s_macierz.html#a817c4079dc927d9f03fa389f00932e82", null ],
    [ "operator*", "class_s_macierz.html#a72c4d6c98e017c9e55bb1eb7f997ae78", null ],
    [ "operator*", "class_s_macierz.html#a97b7c6c3eedfd035ea4c83ffc87c0142", null ],
    [ "SprowadzDoTrojkatnej", "class_s_macierz.html#a36a96fa1301e7211bf4294856c2e9f08", null ],
    [ "Transponuj", "class_s_macierz.html#a1f59aed61694f6f86e72a04b00f2ed39", null ],
    [ "UczynJednostkowa", "class_s_macierz.html#af3be71adb524c1490cfb9542c704c1a5", null ],
    [ "WstawKolumne", "class_s_macierz.html#a87fcc5e59a754d6e1d57647c10a73ee5", null ],
    [ "WstawWiersz", "class_s_macierz.html#a4e69ba4dec19b1c4595cd8b9d3a0747e", null ],
    [ "Wyznacznik", "class_s_macierz.html#a6ee31b14218b62260f6cee8ef809c604", null ],
    [ "_Wiersz", "class_s_macierz.html#a167426334db7782cd2460f0188092b87", null ]
];