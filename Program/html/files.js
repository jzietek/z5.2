var files =
[
    [ "dron.cpp", "dron_8cpp.html", null ],
    [ "dron.hh", "dron_8hh.html", [
      [ "dron", "classdron.html", "classdron" ]
    ] ],
    [ "lacze_do_gnuplota.cpp", "lacze__do__gnuplota_8cpp.html", "lacze__do__gnuplota_8cpp" ],
    [ "lacze_do_gnuplota.hh", "lacze__do__gnuplota_8hh.html", "lacze__do__gnuplota_8hh" ],
    [ "MacierzRot3D.cpp", "_macierz_rot3_d_8cpp.html", "_macierz_rot3_d_8cpp" ],
    [ "MacierzRot3D.hh", "_macierz_rot3_d_8hh.html", "_macierz_rot3_d_8hh" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "obrys.cpp", "obrys_8cpp.html", "obrys_8cpp" ],
    [ "obrys.hh", "obrys_8hh.html", "obrys_8hh" ],
    [ "powierzchnia.cpp", "powierzchnia_8cpp.html", "powierzchnia_8cpp" ],
    [ "powierzchnia.hh", "powierzchnia_8hh.html", "powierzchnia_8hh" ],
    [ "powierzchniageom.cpp", "powierzchniageom_8cpp.html", null ],
    [ "powierzchniageom.hh", "powierzchniageom_8hh.html", [
      [ "powierzchniageom", "classpowierzchniageom.html", "classpowierzchniageom" ]
    ] ],
    [ "pret.cpp", "pret_8cpp.html", null ],
    [ "pret.hh", "pret_8hh.html", [
      [ "pret", "classpret.html", "classpret" ]
    ] ],
    [ "prostopadloscian.cpp", "prostopadloscian_8cpp.html", "prostopadloscian_8cpp" ],
    [ "prostopadloscian.hh", "prostopadloscian_8hh.html", "prostopadloscian_8hh" ],
    [ "scena.cpp", "scena_8cpp.html", null ],
    [ "scena.hh", "scena_8hh.html", [
      [ "scena", "classscena.html", "classscena" ]
    ] ],
    [ "SMacierz.hh", "_s_macierz_8hh.html", "_s_macierz_8hh" ],
    [ "sruba.cpp", "sruba_8cpp.html", "sruba_8cpp" ],
    [ "sruba.hh", "sruba_8hh.html", "sruba_8hh" ],
    [ "SWektor.hh", "_s_wektor_8hh.html", "_s_wektor_8hh" ],
    [ "Wektor3D.hh", "_wektor3_d_8hh.html", [
      [ "Wektor3D", "class_wektor3_d.html", "class_wektor3_d" ]
    ] ],
    [ "woda.cpp", "woda_8cpp.html", "woda_8cpp" ],
    [ "woda.hh", "woda_8hh.html", "woda_8hh" ]
];