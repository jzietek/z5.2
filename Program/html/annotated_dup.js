var annotated_dup =
[
    [ "PzG", "namespace_pz_g.html", "namespace_pz_g" ],
    [ "dron", "classdron.html", "classdron" ],
    [ "MacierzRot3D", "class_macierz_rot3_d.html", "class_macierz_rot3_d" ],
    [ "obrys", "classobrys.html", "classobrys" ],
    [ "powierzchnia", "classpowierzchnia.html", "classpowierzchnia" ],
    [ "powierzchniageom", "classpowierzchniageom.html", "classpowierzchniageom" ],
    [ "pret", "classpret.html", "classpret" ],
    [ "prostopadloscian", "classprostopadloscian.html", "classprostopadloscian" ],
    [ "scena", "classscena.html", "classscena" ],
    [ "SMacierz", "class_s_macierz.html", "class_s_macierz" ],
    [ "sruba", "classsruba.html", "classsruba" ],
    [ "SWektor", "class_s_wektor.html", "class_s_wektor" ],
    [ "Wektor3D", "class_wektor3_d.html", "class_wektor3_d" ],
    [ "woda", "classwoda.html", "classwoda" ]
];