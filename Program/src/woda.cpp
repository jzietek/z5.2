#include "woda.hh"

/**
 * @brief Przeciążenie operatora wczytywania.
 * 
 * @param StrWej 
 * @param wo 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, woda & wo){
    Wektor3D wek;
    for (int i = 0; i < 257; i++){
        StrWej >> wek;
        wo.PunktyGlob.push_back(wek);
    }
    
    return StrWej;
}


/**
 * @brief Metoda obliczająca średnią wysokość - z.
 * 
 * @return double 
 */
double woda::SrednieZ(){
    double zmax, zmin, pom;
    zmax=this->PunktyGlob[0][2];
    zmin=this->PunktyGlob[0][2];
    for (int i = 1; i < 257; i++){
        pom=this->PunktyGlob[i][2];
        if(pom > zmax){
            zmax=pom;
        }
        if (pom < zmin){
            zmin=pom;
        }
        
    }
    return (zmax+zmin)/2;
}

/**
 * @brief Metoda sprawdzająca, czy dron jest kierowany ponad powierzchnię wody.
 * 
 * @param Dron 
 * @return true 
 * @return false 
 */
bool woda::CzyWyplywa(dron & Dron){
    double z;
    z=this->SrednieZ();
    
    if (Dron.ObliczSrodekDrona()[2]>=z){
        return true;
    }
    return false;
}