#include "scena.hh"

/**
 * @brief Metoda wczytuje obiekty, które później będa wyrysowane, oprócz przeszkód.
 * 
 * @return std::fstream 
 */
std::fstream scena::WczytajBryly(){

std::fstream plik;
plik.open("bryly/dron.dat", std::ios::in);
if (plik.is_open()==0){
  std::cerr << "Wczytywanie nie powiodlo sie." << std::endl;
}

plik >> this->Dron._ElementKorpusu;
plik.close();

plik.open("bryly/LewaŚruba.dat", std::ios::in);
if (plik.is_open()==0){
  std::cerr << "Wczytywanie nie powiodlo sie." << std::endl;
}
plik >> this->Dron.LewaSruba;
plik.close();

plik.open("bryly/PrawaŚruba.dat", std::ios::in);
if (plik.is_open()==0){
  std::cerr << "Wczytywanie nie powiodlo sie." << std::endl;
}
plik >> this->Dron.PrawaSruba;
plik.close();

plik.open("bryly/Woda.dat", std::ios::in);
if (plik.is_open()==0){
  std::cerr << "Wczytywanie nie powiodlo sie." << std::endl;
}
plik >> this->Woda;
plik.close();
    return plik;
}


/**
 * @brief Metoda wypisuje do konkretnych plków wczytane obiekty.
 * 
 * @return std::ofstream 
 */
std::ofstream scena::WypiszBryly(){
    std::ofstream plik;
     plik.open("bryly/dron2.dat", std::ios::out);
        plik << this->Dron._ElementKorpusu;
        plik.close();
  
    plik.open("bryly/PrawaŚruba1.dat", std::ios::out);
        plik << this->Dron.PrawaSruba;
        plik.close();
    
     plik.open("bryly/LewaŚruba1.dat", std::ios::out);
        plik << this->Dron.LewaSruba;
        plik.close();

    return plik;
}


/**
 * @brief Metoda wypisuje menu.
 * 
 */
void scena::WypiszMenu(){
    std::cout << "r - zadaj ruch na wprost" << std::endl;
    std::cout << "o - zadaj zmianę orientacji" << std::endl;
    std::cout << "m - wyswietl menu" << std::endl << std::endl;
    std::cout << "k - koniec dzialania programu" << std::endl << std::endl;
}

/**
 * @brief Metoda dodaje elementy na listę.
 * 
 */
void scena::WczytajListe(){
  this->lista_elem_sceny.push_back(new prostopadloscian("bryly/Przeszkoda1.dat"));
  this->lista_elem_sceny.push_back(new prostopadloscian("bryly/Przeszkoda2.dat"));
  this->lista_elem_sceny.push_back(new prostopadloscian("bryly/Przeszkoda3.dat"));
  this->lista_elem_sceny.push_back(new prostopadloscian("bryly/Przeszkoda4.dat"));
  this->lista_elem_sceny.push_back(new powierzchnia("bryly/Przeszkoda5.dat"));
  this->lista_elem_sceny.push_back(new powierzchnia("bryly/Przeszkoda6.dat"));
  this->lista_elem_sceny.push_back(new powierzchnia("bryly/Dno.dat"));
  this->lista_elem_sceny.push_back(new pret("bryly/Przeszkoda7.dat"));
  this->lista_elem_sceny.push_back(new pret("bryly/Przeszkoda8.dat"));
  }

/**
 * @brief Metoda wczytuje obrys z pliku.
 * 
 */
void scena::WczytajObrys(){
  std::fstream plik;
  plik.open("bryly/Dronplussruby.dat", std::ios::in);
  if (plik.is_open()==0){
    std::cerr << "Wczytywanie nie powiodlo sie." << std::endl;
  }
  plik >> this->dronplussruby;
  plik.close();
}

/**
 * @brief Metoda wypisuje obrys drona do sceny.
 * 
 */
void scena::WypiszObrys(){
  std::ofstream plik;
  plik.open("bryly/Dronplussruby.dat", std::ios::out);
  plik << this->Dron._ElementKorpusu;
  plik << this->Dron.LewaSruba;
  plik << this->Dron.PrawaSruba;
  plik.close();
}

/**
 * @brief Metoda sprawdza, czy dojdzie do zderzenia z przeszkodą.
 * 
 * @return true 
 * @return false 
 */
bool scena::CzyZderzenie(){
  this->WypiszObrys();
  this->WczytajObrys();
  double xmaxd, xmind, ymaxd, ymind, zmaxd, zmind;
  double xmaxp, xminp, ymaxp, yminp, zmaxp, zminp;
  xmaxd=this->dronplussruby.Xmax();
  xmind=this->dronplussruby.Xmin();
  ymaxd=this->dronplussruby.Ymax();
  ymind=this->dronplussruby.Ymin();
  zmaxd=this->dronplussruby.Zmax();
  zmind=this->dronplussruby.Zmin(); 

  for(powierzchniageom *WOB : lista_elem_sceny){
    xmaxp=WOB->Xmax();
    xminp=WOB->Xmin();
    ymaxp=WOB->Ymax();
    yminp=WOB->Ymin();
    zmaxp=WOB->Zmax();
    zminp=WOB->Zmin();


  if ((xmind < xmaxp && xminp < xmaxd) && (ymind < ymaxp && yminp < ymaxd) && (zmind < zmaxp && zminp < zmaxd)){
      std::cout << std::endl << "Nastąpi zderzenie z " << WOB->WezNazweTypuObiektu() << std::endl << std::endl;
      this->dronplussruby.PunktyGlob.clear();
      return 1;
    }
  }
  this->dronplussruby.PunktyGlob.clear();  
  return 0;
}