#include "obrys.hh"

/**
 * @brief Przeciążenie operatora wczytywania obrysu.
 * 
 * @param StrWej 
 * @param ob 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, obrys & ob){
    std::fstream plik;
    SWektor <double, 3> wek;
    plik.open("bryly/Dronplussruby.dat", std::ios::in);
    for (int i = 0; i < 82; i++){
        StrWej >> wek;
        ob.PunktyGlob.push_back(wek);
    }
    plik.close();
    return StrWej;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
double obrys::Xmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
double obrys::Xmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Y.
     * 
     * @return double 
     */
double obrys::Ymin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
double obrys::Ymax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
double obrys::Zmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
double obrys::Zmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}