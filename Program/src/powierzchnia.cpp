#include "powierzchnia.hh"

std::fstream &operator >>(std::fstream &StrWej, powierzchnia & po){
    SWektor <double, 3> wek;
    for (int i = 0; i < 257; i++){
        StrWej >> wek;
        po.PunktyGlob.push_back(wek);
    }
    
    return StrWej;
}

double powierzchnia::Xmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

double powierzchnia::Xmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

double powierzchnia::Ymin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

double powierzchnia::Ymax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

double powierzchnia::Zmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}

double powierzchnia::Zmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}