#include "pret.hh"

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
double pret::Xmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
double pret::Xmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Y.
     * 
     * @return double 
     */
double pret::Ymin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
double pret::Ymax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
double pret::Zmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
double pret::Zmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}