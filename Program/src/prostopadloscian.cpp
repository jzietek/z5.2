#include "prostopadloscian.hh"

/**
 * @brief Przeciążenie operatora wczytania prostopadloscianu.
 * 
 * @param StrWej 
 * @param pr 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, prostopadloscian & pr){
    SWektor<double, 3> wek;
    int counter=1;
    for (int i = 0; i <= 9; i++){
        if (counter!=5)
        {
            StrWej >> wek;
            pr.PunktyGlob.push_back(wek);
        }else
        {
            StrWej >> wek;
        }
           
        counter++;
    }
    
    return StrWej;
}

/**
 * @brief Przeciążenie wypisania prostopadloscianu do pliku.
 * 
 * @param StrWyj 
 * @param pr 
 * @return std::ofstream& 
 */
std::ofstream &operator <<(std::ofstream &StrWyj, const prostopadloscian & pr){
    int counter = 1;
    for (int i = 0; i < 8; i++){
        if (counter != 5)
        {
            StrWyj << pr.PunktyGlob[i] << std::endl;
        }else
        {
            StrWyj << pr.PunktyGlob[0] << std::endl;
            i--;
        }
        counter++;
    }
    
    StrWyj << pr.PunktyGlob[4] << std::endl;
    StrWyj << pr.PunktyGlob[7] << std::endl;
    StrWyj << pr.PunktyGlob[3] << std::endl;
    StrWyj << pr.PunktyGlob[2] << std::endl;
    StrWyj << pr.PunktyGlob[6] << std::endl;
    StrWyj << pr.PunktyGlob[5] << std::endl;
    StrWyj << pr.PunktyGlob[1];

    return StrWyj;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
double prostopadloscian::Xmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
double prostopadloscian::Xmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Y.
     * 
     * @return double 
     */
double prostopadloscian::Ymin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
double prostopadloscian::Ymax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
double prostopadloscian::Zmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
double prostopadloscian::Zmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}