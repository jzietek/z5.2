#include "powierzchniageom.hh"

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy X.
     * 
     * @return double 
     */
double powierzchniageom::Xmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy X.
     * 
     * @return double 
     */
double powierzchniageom::Xmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[0]){
        pom=WOB[0];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Y.
     * 
     * @return double 
     */
double powierzchniageom::Ymin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Y.
     * 
     * @return double 
     */
double powierzchniageom::Ymax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[1]){
        pom=WOB[1];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca najmniejszy Z.
     * 
     * @return double 
     */
double powierzchniageom::Zmin(){
    double pom= 2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom>WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}

/**
     * @brief Metoda wirtualna, zwracająca największy Z.
     * 
     * @return double 
     */
double powierzchniageom::Zmax(){
    double pom= -2000 ;
    for( Wektor3D &WOB : PunktyGlob ){
      if (pom<WOB[2]){
        pom=WOB[2];
      }}
    return pom;
}