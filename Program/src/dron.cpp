#include "dron.hh"

/**
 * @brief Metoda oblicza środek drona.
 * 
 * @return SWektor<double, 3> 
 */
SWektor<double, 3> dron::ObliczSrodekDrona(){
    SWektor<double, 3> wek;
    int counter=0;
    for (int i = 0; i < 8; i++){
        wek = wek + this->_ElementKorpusu.PunktyGlob[i];
        counter++;
    }
    return wek/counter;
}

/**
 * @brief Metoda obraca drona wraz ze śrubami względem osi OZ o zadany kąt.
 * 
 * @param kat 
 */
void dron::ObrotOZ(const double kat){
    MacierzRot3D mat;
    this->_KatOZ=(this->_KatOZ+kat*M_PI/180);
    mat.UstawRotZ_st(kat);

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i]=this->LewaSruba.cialo[i]-this->_srodekdrona;
        this->PrawaSruba.cialo[i]=this->PrawaSruba.cialo[i]-this->_srodekdrona;
    }

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i]=this->_ElementKorpusu.PunktyGlob[i]-this->_srodekdrona;
    }

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i]=mat*this->_ElementKorpusu.PunktyGlob[i];
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i]=mat*this->LewaSruba.cialo[i];
        this->PrawaSruba.cialo[i]=mat*this->PrawaSruba.cialo[i];
       
    }

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i]=this->_ElementKorpusu.PunktyGlob[i]+this->_srodekdrona;
    }
     for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i]=this->LewaSruba.cialo[i]+this->_srodekdrona;
        this->PrawaSruba.cialo[i]=this->PrawaSruba.cialo[i]+this->_srodekdrona;}

}


/**
 * @brief Metoda pozwala przesuwać się dronowi wraz ze śrubami na zadanę odległość, pod zadanym kątem, uwzględniając obrót względem osi OZ.
 * 
 * @param odleglosc 
 * @param kat 
 */
void dron::Przesuniecie(const double odleglosc, const double kat){
    MacierzRot3D mat, mat2;
    SWektor<double, 3> wek;
    mat.UstawRotZ_st(this->_KatOZ*180/M_PI);
    mat2.UstawRotY_st(-kat);
    wek[0] = odleglosc;
    wek[1] = 0;
    wek[2] = 0;

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i] =this->_ElementKorpusu.PunktyGlob[i] + mat*(mat2*wek);
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i] = LewaSruba.cialo[i] + mat*(mat2*wek);
        this->PrawaSruba.cialo[i] = PrawaSruba.cialo[i] + mat*(mat2*wek);
    }
}

/**
 * @brief Metoda przesuwania drona po powierzchni wody.
 * 
 * @param odleglosc 
 * @param kat 
 * @param z 
 */
void dron::Wyplyniecie(const double odleglosc, const double kat, const double z){
    MacierzRot3D mat, mat2;
    SWektor<double, 3> wek;
    double srodek;
    mat.UstawRotZ_st(this->_KatOZ*180/M_PI);
    mat2.UstawRotY_st(-kat);
    wek[0] = odleglosc;
    wek[1] = 0;
    wek[2] = 0;

    srodek=this->ObliczSrodekDrona()[2];

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i] =this->_ElementKorpusu.PunktyGlob[i] + mat*(mat2*wek);
    }

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i][2] =this->_ElementKorpusu.PunktyGlob[i][2] - (srodek-z);
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i] = LewaSruba.cialo[i] + mat*(mat2*wek);
        this->PrawaSruba.cialo[i] = PrawaSruba.cialo[i] + mat*(mat2*wek);
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i][2] = LewaSruba.cialo[i][2] - (srodek-z);
        this->PrawaSruba.cialo[i][2] = PrawaSruba.cialo[i][2] - (srodek-z);
    }
}


/**
 * @brief Metoda służąca drugiemu typowi poruszania się, odpowiada za poruszanie się w dół, lub w górę.
 * 
 * @param odleglosc 
 * @param kat 
 */
void dron::DoGory(const double odleglosc, const double kat){
    MacierzRot3D mat, mat2;
    SWektor<double, 3> wek;
    mat.UstawRotZ_st(this->_KatOZ*180/M_PI);
    mat2.UstawRotY_st(-kat);
    wek[0] = odleglosc;
    wek[1] = 0;
    wek[2] = 0;

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i] =this->_ElementKorpusu.PunktyGlob[i] + mat*(mat2*wek);
    }

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i][0] =this->_ElementKorpusu.PunktyGlob[i][0] - (mat*(mat2*wek))[0];
        this->_ElementKorpusu.PunktyGlob[i][1] =this->_ElementKorpusu.PunktyGlob[i][1] - (mat*(mat2*wek))[1];
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i] = LewaSruba.cialo[i] + mat*(mat2*wek);
        this->PrawaSruba.cialo[i] = PrawaSruba.cialo[i] + mat*(mat2*wek);
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i][0] = LewaSruba.cialo[i][0] - (mat*(mat2*wek))[0];
        this->LewaSruba.cialo[i][1] = LewaSruba.cialo[i][1] - (mat*(mat2*wek))[1];
        this->PrawaSruba.cialo[i][0] = PrawaSruba.cialo[i][0] - (mat*(mat2*wek))[0];
        this->PrawaSruba.cialo[i][1] = PrawaSruba.cialo[i][1] - (mat*(mat2*wek))[1];
    }
}

/**
 * @brief Metoda służąca drugiemu typowi poruszania się, odpowiada za poruszanie się na boki.
 * 
 * @param odleglosc 
 * @param kat 
 */
void dron::NaBoki(const double odleglosc, const double kat){
    MacierzRot3D mat, mat2;
    SWektor<double, 3> wek;
    mat.UstawRotZ_st(this->_KatOZ*180/M_PI);
    mat2.UstawRotY_st(-kat);
    wek[0] = odleglosc;
    wek[1] = 0;
    wek[2] = 0;

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i] =this->_ElementKorpusu.PunktyGlob[i] + mat*(mat2*wek);
    }

    for (int i = 0; i < 8; i++){
        this->_ElementKorpusu.PunktyGlob[i][2] =this->_ElementKorpusu.PunktyGlob[i][2] - (mat*(mat2*wek))[2];
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i] = LewaSruba.cialo[i] + mat*(mat2*wek);
        this->PrawaSruba.cialo[i] = PrawaSruba.cialo[i] + mat*(mat2*wek);
    }

    for (int i = 0; i < 13; i++){
        this->LewaSruba.cialo[i][2] = LewaSruba.cialo[i][2] - (mat*(mat2*wek))[2];
        this->PrawaSruba.cialo[i][2] = PrawaSruba.cialo[i][2] - (mat*(mat2*wek))[2];
    }
}