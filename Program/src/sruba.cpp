#include "sruba.hh"

/**
 * @brief Przeciążenie operatora, służące do wcztywania śrub z plików.
 * 
 * @param StrWej 
 * @param sr 
 * @return std::fstream& 
 */
std::fstream &operator >>(std::fstream &StrWej, sruba & sr){
    SWektor<double, 3> wek;
    int counter = 0;
    for (int i = 0; i < 12; i++){
        counter++;
        if (counter!=7){
            StrWej >> wek;
            sr.cialo.push_back(wek);
        }else{
            StrWej >> wek;
            --i;
        }
    }
    wek = (sr.cialo[6]+sr.cialo[7]+sr.cialo[8]+sr.cialo[9]+sr.cialo[10]+sr.cialo[11])/6;
    sr.cialo.push_back(wek);
    return StrWej;
}

/**
 * @brief Przeciążenie operatora, służące do wypisywania śrub do plików.
 * 
 * @param StrWyj 
 * @param sr 
 * @return std::ofstream& 
 */
std::ofstream &operator <<(std::ofstream &StrWyj, const sruba & sr){
    int counter = 0;
    for (int i = 0; i < 12; i++){
        counter++;
        if (counter!=7){
            StrWyj << sr.cialo[i] << std::endl;
        }else{
            --i;
            StrWyj << sr.cialo[0] << std::endl;
        }
    }
    StrWyj << sr.cialo[6] << std::endl;
    StrWyj << sr.cialo[11] << std::endl;
    StrWyj << sr.cialo[5] << std::endl;
    StrWyj << sr.cialo[4] << std::endl;
    StrWyj << sr.cialo[10] << std::endl;
    StrWyj << sr.cialo[9] << std::endl;
    StrWyj << sr.cialo[3] << std::endl;
    StrWyj << sr.cialo[2] << std::endl;
    StrWyj << sr.cialo[8] << std::endl;
    StrWyj << sr.cialo[7] << std::endl;
    StrWyj << sr.cialo[1] << std::endl;
    StrWyj << sr.cialo[7] << std::endl;
    StrWyj << sr.cialo[12] << std::endl;
    StrWyj << sr.cialo[6] << std::endl;
    StrWyj << sr.cialo[11] << std::endl;
    StrWyj << sr.cialo[12] << std::endl;
    StrWyj << sr.cialo[10] << std::endl;
    StrWyj << sr.cialo[9] << std::endl;
    StrWyj << sr.cialo[12] << std::endl;
    StrWyj << sr.cialo[8];
    return StrWyj;
}