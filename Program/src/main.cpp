#include <iostream>
#include <iomanip>
#include <assert.h>
#include "lacze_do_gnuplota.hh"

#include "prostopadloscian.hh"
#include "dron.hh"
#include "scena.hh"

#include<unistd.h>

using namespace std;

/**
 * @brief Funkcja wyświetla informacje, o liczbie wektrów.
 * 
 */
void WyswietlIloscWek(){
  cout << "Aktualna ilość wektorów: " << endl;
  cout << Wektor3D::WezIloscObiektow_Aktualna() << endl;
  cout << "Łączna ilość wektorów: " << endl;
  cout << Wektor3D::WezIloscObiektow_Laczna() << endl;
}

int main(){
PzG::LaczeDoGNUPlota  Lacze;
scena scena;

scena.WczytajBryly();
scena.WypiszBryly();

Lacze.DodajNazwy();
Lacze.ZmienTrybRys(PzG::TR_3D);
Lacze.Inicjalizuj();  // Tutaj startuje gnuplot.

Lacze.UstawZakresX(-200, 200); //-200;200
Lacze.UstawZakresY(-200, 200); //-200;200
Lacze.UstawZakresZ(-100, 100); //-100;100


Lacze.UstawRotacjeXZ(75,25); // Tutaj ustawiany jest widok

Lacze.Rysuj();        // Teraz powinno pojawic sie okienko gnuplota

int info;
scena.Dron._srodekdrona=scena.Dron.ObliczSrodekDrona();
scena.WczytajListe();

  char t;
  do {
    scena.WypiszMenu();
    WyswietlIloscWek();
    cin >> t;
    switch (t){
      case 'm':
      
      scena.WypiszMenu();
        break;
      
      case 'r':

        double zasieg, katOZ, z, pom;
        z=scena.Woda.SrednieZ();
        cout << "Odleglosc przesuniecia drona: ";
        cin >> zasieg;
        cout << "Pod jakim katem?" << endl;
        cin >> katOZ;
        cout << "Wybierz tryb przemieszczenia: '1' - normalne przemieszczenie, '2'-rozbite na przemieszenie wzdłuż OZ, pózniej na boki." << endl;
        cin >> pom;
        if (pom == 1){
        for(int i = 0; i < 40; i++){
        if (scena.Woda.CzyWyplywa(scena.Dron)==1){
          scena.Dron.Wyplyniecie(zasieg/40, katOZ, z);
        }else{
        scena.Dron.Przesuniecie(zasieg/40, katOZ);}
        if (scena.CzyZderzenie()==0){
          scena.WypiszBryly();
          Lacze.Rysuj();
          usleep(50000);      
        }else{
          scena.Dron.Przesuniecie(-zasieg/40, katOZ);
          scena.WypiszBryly();
          Lacze.Rysuj();
          usleep(50000);
          i=40;
        }}}

        if (pom == 2){
        for(int i = 0; i < 40; i++){
        if (scena.Woda.CzyWyplywa(scena.Dron)==1){
          scena.Dron.Wyplyniecie(zasieg/40, katOZ, z);
        }else{
        scena.Dron.DoGory(zasieg/40, katOZ);}
        if (scena.CzyZderzenie()==0){
          info = scena.CzyZderzenie();
          scena.WypiszBryly();
          Lacze.Rysuj();
          usleep(50000);      
        }else{
          info = scena.CzyZderzenie();
          scena.Dron.Przesuniecie(-zasieg/40, katOZ);
          scena.WypiszBryly();
          Lacze.Rysuj();
          usleep(50000);
          i=40;
        }
        }
        for(int i = 0; i < 40; i++){
        scena.Dron.NaBoki(zasieg/40, katOZ);
        if (info==0){
          scena.WypiszBryly();
          Lacze.Rysuj();
          usleep(50000);      
        }else{
          scena.Dron.Przesuniecie(-zasieg/40, katOZ);
          scena.WypiszBryly();
          Lacze.Rysuj();
          usleep(50000);
          i=40;
        }
        }}
        if (scena.Woda.CzyWyplywa(scena.Dron)==1){
          cerr << "Dron podwodny wypłynął na powierzchnię wody." << endl;
        }
        
        
        break;

      case 'o':

        double katOY;
        cout << "Podaj wartosc kata: ";
        cin >> katOY;
        for (int i = 0; i < 20; i++){
          scena.WczytajBryly();
          if (scena.CzyZderzenie()==0){
            scena.Dron._srodekdrona=scena.Dron.ObliczSrodekDrona();
          }
          scena.Dron.ObrotOZ(katOY/20);
        if (scena.CzyZderzenie()==0){
          scena.WypiszBryly();
          Lacze.Rysuj();
          usleep(50000);      
        }else{
          scena.Dron.ObrotOZ(-katOY/20);
          i=20;
        }
        
          }
        break;

      default:
        cout << "Koniec dzialania programu" << endl;
        t = 'k';
        break;

    }

  } while (t != 'k');
}